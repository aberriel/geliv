package Model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author José Alexandre
 */
@Entity
public class Livro implements Serializable
{
    /** Identificador único do registro no banco */
    @Id
    @GeneratedValue
    private long id;
    /** GET para o identificador único do registro no banco */
    public long getId() { return id; }
    /** SET para o identificador único do registro no banco */
    public void setId(long id) { this.id = id; }

    /** Título do livro */
    private String titulo;
    /** GET para o título do livro */
    public String getTitulo() { return titulo; }
    /** SET para o título do livro */
    public void setTitulo(String titulo) { this.titulo = titulo; }

    /** Nome do autor */
    private String autor;
    /** GET para o nome do autor */
    public String getAutor() { return autor; }
    /** SET para o nome do autor */
    public void setAutor(String autor) { this.autor = autor; }

    /** Total de páginas */
    private int paginas;
    /** GET para o total de páginas */
    public int getPaginas() { return paginas; }
    /** SET para o total de páginas */
    public void setPaginas(int paginas) { this.paginas = paginas; }

    /** Nome da editora */
    private String editora;
    /** GET para o nome da editora */
    public String getEditora() { return editora; }
    /** SET para o nome da editora */
    public void setEditora(String editora) { this.editora = editora; }

    /** Código ISBN */
    private String isbn;
    /** GET para o código ISBN */
    public String getIsbn() { return isbn; }
    /** SET para o código ISBN */
    public void setIsbn(String isbn) { this.isbn = isbn; }

    private int avaliacao;
    public int getAvaliacao() { return avaliacao; }
    public void setAvaliacao(int avaliacao) { this.avaliacao = avaliacao; }

    /** Construtor */
    public Livro() { }

    public Livro(long id, String titulo, String autor, int paginas, String editora, String isbn, int avaliacao)
    {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
        this.editora = editora;
        this.isbn = isbn;
        this.avaliacao = avaliacao;
    }
}