package Dao;

import Model.Livro;
import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author José Alexandre
 */
public class LivroDaoImp implements LivroDao
{
    /**
     * Insere livro no banco
     * @param livro Dados do livro a ser salvo
     */
    public void save(Livro livro)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.save(livro);
        t.commit();
    }
    
    /**
     * Localiza um livro pelo seu id
     * @param id Id do libro a ser buscado
     * @return Dados do livro encontrado
     */
    public Livro getLivro(long id)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (Livro) session.load(Livro.class, id);
    }
    
    /**
     * Lista todos os livros registrados na base
     * @return Listagem de livros encontrados
     */
    public List<Livro> list()
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        List lista = session.createQuery("from Livro").list();
        t.commit();
        return lista;
    }
    
    /**
     * Remove um livro da base
     * @param livro Dados do livro a ser removido
     */
    public void remove(Livro livro)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.delete(livro);
        t.commit();
    }
    
    /**
     * Atualiza dados de livro no banco
     * @param livro Dados atualizados do livro
     */
    public void update(Livro livro)
    {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction t = session.beginTransaction();
        session.update(livro);
        t.commit();
    }
}