package Dao;

import Model.Livro;
import java.util.List;

/**
 * Camada de persistência de dados para livros.
 * 
 * DAO = Data Access Object
 * 
 * @author José Alexandre
 */
public interface LivroDao
{
    /**
     * Registra livro na base
     * @param livro 
     */
    public void save(Livro livro);
    
    /**
     * Recupera dados de livro do banco
     * @param id Id do livro a ser buscado
     * @return Dados do livro encontrado
     */
    public Livro getLivro(long id);
    
    /**
     * Resgata todos os livros registrados no banco
     * @return Listagem de livros encontrados
     */
    public List<Livro> list();
    
    /**
     * Apaga libro da base
     * @param livro Dados do livro a ser apagado
     */
    public void remove(Livro livro);
    
    /**
     * Atualiza dados de livro no banco
     * @param livro Dados do livro atualizados
     */
    public void update(Livro livro);
}